 $(document).ready(function () {
		var oscarData = [];
		$.ajax({url: "oscars.json", success: function(result){
			// https://www.htmlgoodies.com/beyond/css/working_w_tables_using_jquery.html
			  oscarData.push(result);
			  var yearsChoice = oscarData[0].filter(function(x) {
				return x.Year
			  });
			  // Populate the form Exact Year select box with year values taken from the json data
			  const uniqueArr = [... new Set(yearsChoice.map(data => data.Year))]
			  uniqueArr.forEach(key => 	$('#exactYear').append(`'<option value="${key}">${key}</option>'`))
		}.bind(this)});	

		var form = document.getElementById("searchOscars").onsubmit=function(e){
			 e.preventDefault();
			 var spinner = document.getElementById('spinner');
			 spinner.innerHTML = '<i class="fa fa-spinner fa-spin" style="font-size:44px"></i>'
			 // Get the selected values from the submitted form
			 var data = new FormData(this);
			 var formValuesList = [];
			 var resultsDisplayAmount = 10;
			 for (const [key,value] of data) {
				 if (value !== "disabled" && key !== 'Results' && value) {
				  formValuesList.push({
					 [key]: value
					})
				 }
				 if (key == 'Results') {
					resultsDisplayAmount = value;
				 }
			 }	
			  validationErrors = validate(formValuesList)
			  // If validation passes then search the json data for search terms and display the results
			  if (! validationErrors) {
			  	var searchResults = search(data);
				displayResults(searchResults, resultsDisplayAmount);		
			  }
		};

		// Function that gets json data and searches for requested search terms
		function search(searchTerms) {
			 const [ exactYear, year, category, nominations, nominee, info, both ] = searchTerms;
			 // If exact Year has a value then use it
			 var newYear = (exactYear[1] === 'disabled') ? year[1]: exactYear[1];
			 var nomination = (nominations[1] === 'disabled') ? '': nominations[1];
			 var infoNom = ''
			 // If the (Nominee or Info) field has a value, use it in the Nominee and the Info value
			 if (both[1] !== 'disabled' && both[1] !== '') {
				nominations[1] = both[1]
				info[1] = both[1];
			 }
			 var results = oscarData[0].filter(function(x, i) {
				return (x.Info.toString().toLowerCase().includes(info[1].toLowerCase())
						&& x.Nominee.toString().toLowerCase().includes(nominee[1].toLowerCase())
						&& x.Category.toString().toLowerCase().includes(category[1].toLowerCase())
						&& x.Won.includes(nomination) && x.Year.includes(newYear)
						)
			  }, []);
			return results;
		}
		  
		 // Function that creates a table and displays results
		function displayResults(results, resultsAmount) {
			$('.results').empty();
			if (results.length > 0) {
			var heading = '<h2>The Oscars Search Results</h2>';
			var table = $("<table/>").addClass('oscars_table');
			var row = $("<tr/>");
			var data = ['Year', 'Category', 'Nominee', 'Info', 'Won'];
			$.each(data, function(index, val) { 
					row.append($("<th/>").text(val));
			});
			table.append(row);
			$.each(results, function(index, value) {   
				const { Year, Category, Nominee, Info, Won } = value;
				row = $("<tr/>");
				row.append($("<td/>").text(Year));
				row.append($("<td/>").text(Category));
				row.append($("<td/>").text(Nominee));
				row.append($("<td/>").text(Info));
				row.append($("<td/>").text(Won));
				table.append(row);
				if (index === parseInt(resultsAmount, 10) -1) {
					return false;
				}
			})
			$('.results').append(heading);
			$('.results').append(table);
			$('.results').append(`<br><h3>Total results found - ${results.length}`);
			} else {
				$('.results').append('<h2>No results found.</h2>');
			}
			$('#spinner').hide();

 		  }
		  
		//Function to validate inputs
		function validate(formValues) { 
			var formData = [];
			var nomineeAndInfoError = document.getElementById("nominee_error")
			nomineeAndInfoError.innerHTML = '';
			var yearError = document.getElementById("year_error")
			yearError.innerHTML = '';
			let validationResults = formValues.filter((x) => {
					var validation = true; 
					if (typeof x.exactYear !== 'undefined') {
						formData.push(Object.keys(x).toString());
					}
					if (typeof x.Year !== 'undefined') {
						formData.push(Object.keys(x).toString())
					}
					if (typeof x.Info !== 'undefined') {
						formData.push(Object.keys(x).toString());
					}
					if (typeof x.Nominee !== 'undefined') {
						formData.push(Object.keys(x).toString())
					}
					if (typeof x.both !== 'undefined') {
						formData.push(Object.keys(x).toString())
					}
					// Display errors if any
					if (formData.includes('Year') && formData.includes('exactYear')) {
						yearError.innerHTML = 'Year or Exact Year - not both!'
						validation = false;
					}
					if ((formData.includes('Info') && formData.includes('both')) || (formData.includes('Nominee') && formData.includes('both'))) {
						nomineeAndInfoError.innerHTML = 'Can\'t use this with either - Nominee \/ Info'
						validation = false;
					}
					$('#spinner').hide();

					return validation
			});
			return formValues.length >  validationResults.length
		}
});