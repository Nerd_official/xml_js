<!doctype html>
<html>
<style>
/**https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_table_responsive **/
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}


.error {
	color: red;
	padding-left: .3rem;
}
.creator {
	margin-top: 6.25rem;
}
</style>


<?php
$xml = simplexml_load_file('cia.xml');
 
$arr = array();

$root = '//countries/country';
// Initialize variables
$open_bracket = '[';
$close_bracket = ']';
$lang_query = '';
$query = '';
$people_query = '';
$rate_query = '';
$lang_header = $total_header = $birth_header = $death_header = '';
$error = "";
$error_count = 0;
$results = [];
$submitted = false;
$no_results = '';

$empty_birth_rate = $empty_death_rate = $empty_greater_birth = $empty_total = $empty_lang = true;
if (isset($_GET['totalVal']) && isset($_GET['greater']) && !empty($_GET["totalVal"]) && $_GET['greater'] != 'none' && $_GET['greater'] != '') 
{ 
    $total_header = '<th>Total</th>';
	$operator = $_GET['greater'];
	$value = $_GET['totalVal'];
	$empty_total = empty($_GET["totalVal"]);
    $people_query = "people/population/total and people/population/total/text()". " " . $operator . " '" . $value . "'";
	array_push($arr, $people_query);
} 
else if (isset($_GET['total']) && !empty($_GET["total"]) && $_GET['greater'] != 'none') {
	$total_header = '<th>Total</th>';
	$empty_total = empty($_GET["total"]);
	$people_query = 'people/population/total';
	array_push($arr, $people_query);
}
if (isset($_GET['birth']) && !empty($_GET["birth"])) 
{ 
    $birth_header = '<th>Birth Rate</th>';
	$empty_birth_rate = empty($_GET["birth"]);
	$birth_rate_query = 'people/population/birth/text()';
	array_push($arr, $birth_rate_query);
}
if (isset($_GET['death']) && !empty($_GET["death"])) 
{ 
	$death_header = '<th>Death Rate</th>';
    $empty_death_rate = empty($_GET["death"]);
	$death_rate_query = 'people/population/death/text()';
	array_push($arr, $death_rate_query);
}
if (isset($_GET['greaterBirth']) && !empty($_GET["greaterBirth"]))
{ 
	// Check if Birth Rate and Death rate have been checked - If not set error count
	if (empty($_GET["death"]) && empty($_GET["birth"])) {
		$error_count = 1;
	}
    $empty_birth = empty($_GET["greaterBirth"]);
    $birth_options_operator = $_GET['greaterBirth'];
    $birth_options_query = "people/population[birth/text()". " " . $birth_options_operator . " death/text()]";
	array_push($arr, $birth_options_query);
}
if (isset($_GET['langVal']) && !empty($_GET["langVal"]))
{ 
	$empty_lang = empty($_GET["langVal"]);
	$lang_header = '<th>Language</th>';
	$lang = $_GET['langVal'];
    $lang_query = "people/languages[contains(text(),'" . ucwords($lang) . "')]";
	array_push($arr, $lang_query);
}

if (isset($_GET['submit'])) {
    // If form is submitted with no selections - query to get all results
	if (count($arr) == 0) {
		array_push($arr, $people_query . 'text()');
	} 
	if ($error_count == 0) {
		// Build query string
		$string = rtrim(implode(' and ', $arr), ',');
		$query = $root . $open_bracket . $string . $close_bracket;	    

		$results = $xml->xpath($query);
		if (count($results) == 0) {
			$no_results = '<br><em><b>No results found.</b></em>';
		}
	} else {
		$error = " Please check [Birth Rate or Death Rate.] or both.";
	}
}
?>
<head>
   <meta charset="utf-8">
   <title>PHP - FactBook </title>
   <link rel="stylesheet" type="text/css" href="#">
</head>
<body>
   <h1 id="pageheading">ITW</h1>
   <h2 class="sectionheading">The World Factbook</h2>
   <form id="register" name="form1" action="xml.php" method="GET">
   <fieldset id="formList">
   <div>
    <div class="text">Country Information</div>
      <label for="total">Total:</label>
		<input type="checkbox" name="total" id="total" <?php echo isset($_GET['total']) ? 'checked' : '' ?>>
	  <label for="totalVal">Enter Value:</label>
	    <input type="text" name="totalVal" id="totalVal" value="<?php echo isset($_GET['totalVal']) ? $_GET['totalVal'] : '' ?>">
	  <label for="greater">Greater/Less:</label>
	  <select name="greater" id="greater">
			<option value="" <?php if (isset($_GET['greater']) && $_GET['greater'] == '') { ?>selected="true" <?php }; ?>>Select</option>
			<option value="none" <?php if (isset($_GET['greater']) && $_GET['greater'] == 'none') { ?>selected="true" <?php }; ?>>None</option>
			<option value=">" <?php if (isset($_GET['greater']) && $_GET['greater'] == '>') { ?>selected="true" <?php }; ?>>Greater Than</option>
			<option value="<" <?php if (isset($_GET['greater']) && $_GET['greater'] == '<' ) { ?>selected="true" <?php }; ?>>Less Than</option>
	  </select>
    </div>
	<br>
	<?php
	  if(isset($error) && $error) {
		echo "<p style=\"color: red;\">*",htmlspecialchars($error),"</p>\n\n";
	  }
	?>
	<div>
	 <label for="birth">Birth Rate:</label>
      <input type="checkbox" name="birth" id="birth" <?php echo isset($_GET['birth']) ? 'checked' : '' ?>>
    </div>
  <br>
  <div>
	 <label for="death">Death Rate:</label>
      <input type="checkbox" name="death" id="death" <?php echo isset($_GET['death']) ? 'checked' : '' ?>>
  </div>
	<br>
	 <div>
    <div class="text">Birth Rate Options:</div>
	  <label for="greaterBirth">Greater/Less:</label>
	  <select name="greaterBirth" id="greaterBirth">
			<option value="" <?php if (isset($_GET['greaterBirth']) && $_GET['greaterBirth'] == 'disabled') { ?>selected="true" <?php }; ?>>Select</option>
			<option value=">" <?php if (isset($_GET['greaterBirth']) && $_GET['greaterBirth'] == '>') { ?>selected="true" <?php }; ?>>Birth Rate - Greater Than Death Rate</option>
			<option value="<" <?php if (isset($_GET['greaterBirth']) && $_GET['greaterBirth'] == '<') { ?>selected="true" <?php }; ?>>Birth Rate - Less Than Death Rate</option>
	  </select>
    </div>
	<br>
	<div>
       <label for="langVal">Language:</label>
	    <input type="text" name="langVal" id="langVal" value="<?php echo isset($_GET['langVal']) ? $_GET['langVal'] : '' ?>">
  </div>
  <br>
	<div class="btn-submit" id="btnBind">       
		<input type="submit" name="submit" id="submit" value="Submit" />
	</div>
   </fieldset>
</form>
 <div class="message" id="display">
 </div>
</body>

<?php 
	// Display results
	if ($submitted = true && count($results) > 0) {
		echo "<table><tr><th>Name</th>" . $total_header . $birth_header . $death_header . $lang_header . "</tr>";
			foreach($results as $name) {
			  echo "<tr>";
			  echo "<td>" . ($name->name ? $name->name : "not found") . "</td>\n";
			  echo $name->people->population->total !== null && !$empty_total || !$empty_total ? "<td>" . $name->people->population->total . "</td>\n":'';
			  echo $name->people->population->birth !== null && !$empty_birth_rate || !$empty_greater_birth ? "<td>" . $name->people->population->birth . "</td>\n":'';
			  echo $name->people->population->death !== null && !$empty_death_rate || !$empty_greater_birth ? "<td>" . $name->people->population->death . "</td>\n":'';
			  echo $name->people->languages !== null && !$empty_lang ? "<td>" . $name->people->languages . "</td>\n":'';
			  echo "</tr>";
			}
		echo "</table>";
	}
	else if ($submitted = true
	) {
		// No results found
		echo $no_results;
	}
?>
</html>
